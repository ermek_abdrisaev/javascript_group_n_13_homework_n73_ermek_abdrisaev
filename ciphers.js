const express = require('express');
const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;
const password = 'qwe';

app.get('/', (req, res) => {
    res.send(`<h1 style="color: cornflowerblue; font-size: 40px; text-align: center; margin-top: 30%">
You are on main page...<br>
Please enter "encode" or "decode" and word
</h1>`);
});

app.get('/:password/encode/:crypt', (req, res) => {
    res.send(Vigenere.Cipher(`${req.params.password}`).crypt(`${req.params.crypt}`));
});

app.get('/:password/decode/:crypt', (req, res) => {
    res.send(Vigenere.Decipher(`${req.params.password}`).crypt(`${req.params.crypt}`));
});

app.listen(port, () => {
    console.log('You are on' + port);
});

