const express = require('express');
const app = express();
const port = 8080;

app.get('/', (req, res) =>{
    res.send(`<h1 style="font-size: 40px; margin-left: 30px">You are on main page</h1>`);
});

app.get('/:word', (req, res) =>{
    res.send(`<h1 style="text-align: center; color: indigo; font-size: 60px; margin-top: 25%">${req.params.word}</h1>`);
});

app.listen(port, () =>{
    console.log('Hello' + port);
});